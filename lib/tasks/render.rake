
require_relative '../exchange/renderer'

namespace :render do
  desc 'Render configuration and compose files and keys'
  task :config do
    renderer = Exchange::Renderer.new
    renderer.render_keys
    renderer.render
  end
end
