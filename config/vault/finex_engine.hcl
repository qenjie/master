path "transit/exchange_*" {
  capabilities = [ "read" ]
}
# Decrypt secrets
path "transit/decrypt/exchange_*" {
  capabilities = [ "create", "update" ]
}
# Use key for signing
path "transit/sign/exchange_*" {
  capabilities = ["update"]
}
# Create transit key
path "transit/keys/exchange_*" {
  capabilities = ["create"]
}
# Renew tokens
path "auth/token/renew" {
  capabilities = ["update"]
}
# Lookup tokens
path "auth/token/lookup" {
  capabilities = ["update"]
}
